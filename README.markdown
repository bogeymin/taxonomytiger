[Migrated to Github](https://github.com/bogeymin/taxonomytiger)

# Taxonomy Tiger

An app for creating re-usable data validation lists (aka lookups, enums) based on the 
Wagtail CMS.

## Installation

To install:

	pip install https://bitbucket.org/bogeymin/taxonomytiger/get/master.zip;

Or in your requirements file:

	-e git+https://bitbucket.org/bogeymin/taxonomytiger/get/master.zip

Or in your ``setup.py`` file:

	install_requires=["taxonomytiger"],
	dependency_links=[
		"https://bitbucket.org/bogeymin/taxonomytiger/get/master.zip#egg=taxonomytiger",
	]

## Settings

### INCLUDE_AUDIT_IN_LOOKUPS

Setting this to ``True`` will include the following fields
in the child classes:

- ``added_by``: A foreign key using ``AUTH_USER_MODEL``.
- ``added_dt``: A datetime field that defaults to now.

``added_by`` requires additional work in your views. For example:

	class StatusAdmin(admin.ModelAdmin):
		
		def save_model(request, obj, form, change):
			obj.added_by = request.user
			obj.save()

## Examples

### Create a New Lookup

To create a new char-based lookup:

	from taxonomytiger.models import StringLookupModel
	
	class Status(StringLookupModel):
	
		class Meta:
			ordering = ["label"]
			verbose_name = _("Status")
			verbose_name_plural = _("Statuses")