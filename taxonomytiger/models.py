# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel

# Exports

__all__ = (
    "LookupModel",
    "IntegerLookupModel",
    "StringLookupModel",
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

INCLUDE_AUDIT_IN_LOOKUPS = getattr(
    settings,
    "INCLUDE_AUDIT_IN_LOOKUPS",
    False
)

# Abstract Models


class LookupModel(models.Model):
    """Base class for lookup models."""

    abbr = models.CharField(
        _("abbreviation"),
        blank=True,
        help_text=_("Enter the common abbreviation for the entry, if any."),
        max_length=16,
        null=True
    )

    if INCLUDE_AUDIT_IN_LOOKUPS:
        added_by = models.ForeignKey(
            AUTH_USER_MODEL,
            help_text=_("The user that added the record."),
            related_name="%(app_label)_%(class)s_added",
            verbose_name=_("added by")
        )

        added_dt = models.DateTimeField(
            _("added date/time"),
            auto_now_add=True,
            help_text=_("Date and time the record was created.")
        )

    description = models.TextField(
        blank=True,
        help_text=_("Brief description of the entry."),
        null=True
    )

    label = models.CharField(
        _("label"),
        help_text=_("Official name or title for the entry."),
        max_length=128,
        unique=True
    )

    panels = [
        FieldPanel("label", classname="full title"),
        FieldPanel("abbr"),
        FieldPanel("description", classname="full"),
    ]

    class Meta:
        abstract = True

    def __str__(self):
        return self.label

    def __unicode__(self):
        return u"%s" % self.label

    @property
    def title(self):
        """Alias of label. Needed when using ``register_chooser_for_model()``."""
        return self.label


class IntegerLookupModel(LookupModel):
    """Create a lookup with an integer value."""

    value = models.IntegerField(
        _("value"),
        help_text=_("The value of the lookup."),
        unique=True
    )

    panels = LookupModel.panels + [
        FieldPanel("value"),
    ]

    class Meta:
        abstract = True


class StringLookupModel(LookupModel):
    """Create a lookup with a string value."""

    value = models.CharField(
        _("value"),
        help_text=_("The value of the lookup."),
        max_length=64,
        unique=True
    )

    panels = LookupModel.panels + [
        FieldPanel("value"),
    ]

    class Meta:
        abstract = True
